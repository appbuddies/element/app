<?php

namespace api\controllers;

use api\files\{
    FileStore
};

use app\controllers\{
    BaseController
};

use app\models\upload\{
    Image
};

use Exception;

use Intervention\Image\ImageManager;
use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class ImageController extends BaseController {

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return Response
     */
    public function store(Request $request, Response $response) {

        if (!$upload = $request->getUploadedFiles()['file'] ?? null) {

            $data['error'] = 'Oops! ...it looks like you forgot to add an image?';
            return $response->withJson($data, 422);
        }

        try {

            $this->container->get(ImageManager::class)->make($upload->file);

        } catch (Exception $e) {

            $data['error'] = 'File-type not accepted';
            return $response->withJson($data, 415);
        }

        $store = (new FileStore())->store($upload);

        return $response->withJson([
            'data' => [
                'info' => "Image uploaded to server!...",
                'uuid' => $store->getStored()->uuid
            ]
        ]);
    }

    public function show(Request $request, Response $response, $uuid) {

        try {

            $image = Image::with([])->where('uuid', '=', $uuid)->firstOrFail();

        } catch (Exception $e) {

            return $response->withStatus(404);
        }

        $response->getBody()->write(

            $this->getProcessedImage($image, $request)
        );

        return $this->respondWithHeaders($response);
    }

    protected function getProcessedImage($image, $request) {

        return $this->container->get(ImageManager::class)->cache(function ($builder) use ($image, $request) {

            $this->processImage(
                $builder->make(uploads_image_path($image->uuid)),
                $request
            );
        });
    }

    protected function processImage($builder, $request) {

        return $builder->resize(null, $this->getRequestedSize($request), function ($constraint) {

            $constraint->aspectRatio();

        })->encode('png');
    }

    protected function getRequestedSize($request) {

        return max(min($request->getParam('s'), 800) ?? 100, 10);
    }

    protected function respondWithHeaders($response) {

        foreach ($this->getResponseHeaders() as $header => $value) {

            $response = $response->withHeader($header, $value);
        }

        return $response;
    }

    protected function getResponseHeaders() {

        return [
            'Content-Type' => 'image/png'
        ];
    }
}
