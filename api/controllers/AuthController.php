<?php

namespace api\controllers;

use Interop\Container\ContainerInterface;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use app\controllers\BaseController;
use app\handlers\auth\JwtAuth;

class AuthController {

    protected $auth;

    public function __construct(ContainerInterface $container, JwtAuth $auth) {

        $this->auth = $auth;
    }

    public function fetchToken(Request $request, Response $response) {

        if (!$token = $this->auth->attempt($request->getParam('email'), $request->getParam('password'))) {

            return $response->withStatus(401, 'Fetching Token failed');
        }

        return $response->withJson([
            'token' => $token
        ]);
    }
}