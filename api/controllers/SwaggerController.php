<?php

namespace api\controllers;

use app\controllers\{
    BaseController
};

use Psr\Http\Message\{
    ResponseInterface as Response
};

class SwaggerController extends BaseController {

    /**
     * @param Response $response
     *
     * @return Response
     */
    public function getApiVersion1Ui(Response $response) {

        return $this->view->render($response, '/api/v1/SwaggerUI.twig', [

            // ...
        ]);
    }
}