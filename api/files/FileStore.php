<?php

namespace api\files;

use app\models\upload\Image;

use Exception;

use Slim\Http\UploadedFile;

class FileStore {

    protected $stored = null;

    public function getStored() {

        return $this->stored;
    }

    public function store(UploadedFile $file) {

        try {

            $model = $this->createModel($file);

            $file->moveTo(uploads_image_path($model->uuid));

        } catch (Exception $e) {

            dump($e);
            die;
        }

        return $this;
    }

    protected function createModel(UploadedFile $file) {

        return $this->stored = Image::with([])->create();
    }
}