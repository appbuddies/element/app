<?php

use function DI\get;

use app\handlers\{
    AppVersion,
    auth\Auth,
    auth\JwtAuth,
    auth\JwtClaims,
    auth\JwtFactory,
    auth\Parser,
    errors\NotFoundHandler,
    mailer\Mailer
};

use app\providers\{
    auth\EloquentProvider,
    jwt\FirebaseProvider
};

use app\validations\{
    Validator,
    contracts\ValidatorInterface,
    users\domain\DomainCheck,
    users\email\EmailAvailable,
    users\initials\InitialsAvailable,
    users\password\MatchesPassword,
    users\password\ConfirmPassword
};

use app\views\{
    Factory,
    extensions\TranslationExtension
};

use GuzzleHttp\Client as HttpClient;

use Illuminate\{
    Translation\Translator,
    Translation\FileLoader,
    Filesystem\Filesystem
};

use Intervention\Image\ImageManager;

use Noodlehaus\Config;

use Psr\Container\{
    ContainerInterface
};

use Psr\Http\Message\{
    ServerRequestInterface,
    ResponseInterface
};

use Slim\{
    Csrf\Guard,
    Flash\Messages,
    Router,
    Views\Twig,
    Views\TwigExtension
};

use Symfony\{
    Component\Console\Application,
    Component\EventDispatcher\EventDispatcher
};

use Twilio\Rest\Client as Twilio;

/**
 * attaching : TO CONTAINER ->
 */
return [

    /* 404 ERROR */
    'notFoundHandler' => function(ContainerInterface $container) {

        return new NotFoundHandler ($container->get(Twig::class));
    },

    /* APP -> CONFIGS */
    Config::class => function() {

        switch (getenv("APP_MODE")) {

            case 'development':
                return new Config(
                    __DIR__ . '/../config/app/development'
                );
                break;

            case 'production':
                return new Config(
                    __DIR__ . '/../config/app/production'
                );
                break;

            default:
                return new Config(
                    __DIR__ . '/../config/app/_examples'
                );
        }
    },

    /* AUTH */
    Auth::class => function () {

        return new Auth;
    },

    /* AUTH -> JWT */
    JwtAuth::class => function (ContainerInterface $container) {

        $authProvider = new EloquentProvider();
        $claimsFactory = new JwtClaims(

            $container->get(ContainerInterface::class),
            $container->get('request')
        );

        $jwtProvider = new FirebaseProvider($container->get(ContainerInterface::class));

        $factory = new JwtFactory($claimsFactory, $jwtProvider);

        $parser = new Parser($jwtProvider);

        return new JwtAuth($authProvider, $factory, $parser);
    },

    /* AUTH -> SOCIAL */
//    AuthSocial::class => function () {
//
//        return new AuthSocial;
//    },

    /* Endpoint Monitor -> Symfony EventDispatcher */
    EventDispatcher::class => function (ContainerInterface $container) {

        $dispatcher = new Symfony\Component\EventDispatcher\EventDispatcher();

        $dispatcher->addListener(
            'endpoint.down',
            [new app\listeners\EndpointDownSMSNotification($container->get(Config::class), $container->get(Twilio::class)), 'handle']
        );

        $dispatcher->addListener(
            'endpoint.up',
            [new app\listeners\EndpointUpSMSNotification($container->get(Config::class), $container->get(Twilio::class)), 'handle']
        );

        return $dispatcher;
    },

    /* CSRF */
    Guard::class => function () {

        $guard = new Guard();
        $guard->setPersistentTokenMode(true);

        return $guard;
    },

    /* FLASH */
    Messages::class => function () {

        return new Messages;
    },

    /* Guzzle HttpClient */
    HttpClient::class => function () {

        return new HttpClient();
    },

    /* MAILER */
    Mailer::class => function (ContainerInterface $container) {

        $transport = (new Swift_SmtpTransport($container->get(Config::class)->get('service.ms.host'), $container->get(Config::class)->get('service.ms.port')))
            ->setUsername($container->get(Config::class)->get('service.ms.username'))
            ->setPassword($container->get(Config::class)->get('service.ms.password'));

        $swift = (new Swift_Mailer($transport));

        return (new Mailer($swift, $container->get(Twig::class)))
            ->alwaysFrom($container->get(Config::class)->get('service.ms.from.address'));
    },

    /* ROUTER */
    'router' => get(Router::class),

    /* TRANSLATOR */
    Translator::class => function (ContainerInterface $container) {

        $fallback = $container->get(Config::class)->get('i18n.translations.fallback');

        $loader = new FileLoader(
            new Filesystem(), $container->get(Config::class)->get('i18n.translations.path')
        );

        $translator = new Illuminate\Translation\Translator($loader, $_SESSION['lang'] ?? $fallback);
        $translator->setFallback($fallback);

        return $translator;
    },

    /* TWIG */
    Twig::class => function(ContainerInterface $container) {

        $config = $container->get(Config::class);

        $twig = Factory::getEngine($config);

        $twig->addExtension(new TwigExtension(

            $container->get('router'),
            $container->get('request')->getUri()
        ));

        $twig->addExtension(new TranslationExtension(
            $container->get(Translator::class)
        ));

        $twig->getEnvironment()->addGlobal('session', $_SESSION);

        $twig->getEnvironment()->addGlobal('app_version', $container->get(AppVersion::class)->getVersion());

        $twig->getEnvironment()->addGlobal('flash', $container->get(Messages::class));

        $twig->getEnvironment()->addGlobal('auth', [

            'check' => $container->get(Auth::class)->check(),
            'user' => $container->get(Auth::class)->user(),
        ]);

        return $twig;
    },

    /* TWILIO */
    Twilio::class => function (ContainerInterface $container) {

        $config = $container->get(Config::class)->get('service.ss');

        return new Twilio(
            $config['sid'], $config['token']
        );
    },

    /* UPLOAD -> IMAGES */
    ImageManager::class => function (ContainerInterface $container) {

        $manager = new ImageManager();
        $manager->configure($container->get('settings.images'));

        return $manager;
    },

    /* VALIDATOR */
    ValidatorInterface::class => function () {

        return new Validator;
    },

];