<?php

namespace app\events;

use Illuminate\Translation\Translator;

use app\events\{
    contracts\Handler,
};

class BaseEvent {

    protected $handlers = [];

    /**
     * @param $handlers
     */
    public function attach($handlers) {

        if (is_array($handlers)) {

            foreach ($handlers as $handler) {

                if (!$handler instanceof Handler) {

                    continue;
                }

                $this->handlers[] = $handler;
            }

            return;
        }

        if (!$handlers instanceof Handler) {

            return;
        }

        $this->handlers[] = $handlers;
    }

    /**
     *
     */
    public function dispatch() {

        foreach ($this->handlers as $handler) {

            $handler->handle($this);
        }
    }
}