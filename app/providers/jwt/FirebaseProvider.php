<?php

namespace app\providers\jwt;

use Interop\Container\ContainerInterface;
use Firebase\JWT\JWT;
use Noodlehaus\Config;

class FirebaseProvider implements JwtProviderInterface {

    protected $c;

    /**
     * FirebaseProvider constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {

        $this->c = $container;
        $this->config = $this->c->get(Config::class);
    }

    /**
     * @param array $claims
     * @return string
     */
    public function encode(array $claims) {

        return JWT::encode($claims, $this->config->get('auth.jwt.secret'), 'HS256');
    }

    /**
     * @param $token
     * @return object
     */
    public function decode($token) {

        return JWT::decode($token, $this->config->get('auth.jwt.secret'), ['HS256']);
    }
}