<?php

namespace app\views;

use Noodlehaus\Config;
use Slim\Views\Twig;
use Twig\Error\LoaderError;

class Factory {

    /**
     * Factory dependencies
     */
    protected $view;
    protected $config;

    /**
     * Factory constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config) {

        $this->config = $config;
    }

    public static function getEngine(Config $config) {

        return new Twig(__DIR__ . '/../../src/views', [
            'cache' => $config->get('cache.view'),
            'debug' => $config->get('app.debug')
        ]);
    }

    public function make($view, $data = []) {

        try {

            $this->view = static::getEngine($this->config)->fetch($view, $data);

        } catch (LoaderError $e) {

            // ...do something

        }

        return $this;
    }

    public function render() {

        return $this->view;
    }
}