<?php

namespace app\views\extensions;

use Noodlehaus\Config;
use Twig_Extension;
use Twig_SimpleFunction;

class GetYamlExtension extends Twig_Extension {

    protected $config;

    /**
     * TranslationExtension constructor.
     * @param Config $config
     */
    public function __construct(Config $config) {

        $this->config = $config;
    }

    public function getFunctions() {

        return [
            new Twig_SimpleFunction('get_config', [$this, 'getConfig']),
        ];
    }

    public function getConfig($variable) {

        return $this->config->get($variable);
    }
}