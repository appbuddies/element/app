<?php

namespace app\views\extensions;

use Twig_Extension;
use Twig_SimpleFunction;

class GetAppVersionExtension extends Twig_Extension {

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions() {

        return [
            new Twig_SimpleFunction('app_version', $_SESSION)
        ];
    }
}