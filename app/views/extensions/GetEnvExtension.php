<?php

namespace app\views\extensions;

use Twig_Extension;
use Twig_SimpleFunction;

class GetEnvExtension extends Twig_Extension {

    public function getFunctions() {

        return [
            new Twig_SimpleFunction('get_env', [$this, 'getEnv']),
        ];
    }

    public function getEnv($variable) {

        return env($variable);
    }
}