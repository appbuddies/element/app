<?php

namespace app\views\extensions;

use Illuminate\Translation\Translator;

use Twig_Extension;
use Twig_SimpleFunction;

class TranslationExtension extends Twig_Extension {

    protected $translator;

    /**
     * TranslationExtension constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator) {

        $this->translator = $translator;
    }

    /**
     * @return array|\Twig\TwigFunction[]
     */
    public function getFunctions() {

        return [
            new Twig_SimpleFunction('trans', [$this, 'trans']),
            new Twig_SimpleFunction('trans_choice', [$this, 'transChoice']),
            new Twig_SimpleFunction('locale', [$this, 'locale'])
        ];
    }

    /**
     * @param $key
     * @param array $replace
     * @return array|null|string
     */
    public function trans($key, array $replace = []) {

        return $this->translator->get($key, $replace);
    }

    /**
     * @param $key
     * @param $count
     * @param array $replace
     * @return string
     */
    public function transChoice($key, $count, array $replace = []) {

        return $this->translator->choice($key, $count, $replace);
    }

    /**
     * @return string
     */
    public function locale() {

        return $this->translator->getLocale();
    }
}