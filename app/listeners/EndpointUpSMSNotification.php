<?php

namespace app\listeners;

use Noodlehaus\Config;
use Symfony\Component\EventDispatcher\Event;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class EndpointUpSMSNotification {

    protected $config;
    protected $sms;

    public function __construct(Config $config, Client $sms) {

        $this->config = $config;
        $this->sms = $sms;
    }

    public function handle(Event $event) {

        try {

            $this->sms->messages->create(
                $this->config->get('service.ss.to_number'),
                [
                    'from' => $this->config->get('service.ss.from_number'),
                    'body' => "{$event->endpoint->uri} is UP with status code of {$event->endpoint->status->status_code}",
                ]
            );

        } catch (TwilioException $e) {

            // ...do something

        }
    }
}