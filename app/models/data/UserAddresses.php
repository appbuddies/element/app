<?php

namespace app\models\data;

use Illuminate\Database\Eloquent\Model;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class UserAddresses extends Model {

    /**
     * Making sure, that our model-class is
     * referring to the correct table !?
     */
    protected $table = 'users_addresses';

    /**
     * Specifying which columns, we want to write to...
     */
    protected $fillable = [

        'user_id',
        'address_id',
        'shipping_address'
    ];
}
