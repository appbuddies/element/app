<?php

namespace app\console\commands;

use app\console\Console;
use Interop\Container\ContainerInterface;
use Noodlehaus\Config;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\PhpExecutableFinder;

use Illuminate\Support\ProcessUtils;

use app\console\Commands;

class StartServerCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'app:serve';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Serve the application on the PHP development server';

    /**
     * Handle the command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return mixed
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $output->writeln("<error> {$this->defHost()} </error>");

        $output->writeln("/*\n|-----------------------------------------------------\n| <info>Element dev-server started on:</info>\n|<info> {$this->time()}</info>\n|-----------------------------------------------------\n| Listening on http://{$this->host()}:{$this->port()}\n| Document root is {$this->root()}\n| Press Ctrl-C to quit.\n*/");

        return exec('php -S '.$this->host().':'.$this->port());
    }

    /**
     * Getting the current timestamp.
     *
     * @return string
     */
    protected function time() {

        $currentTime = date('r');

        return $currentTime;
    }

    /**
     * Getting the current timestamp.
     *
     * @return string
     */
    protected function root() {

        $root = getcwd();

        return $root;
    }

    /**
     * Get the host for the command.
     *
     * @return string
     */
    protected function host() {

        //return $this->input->getOption('host');

        if($this->input->getOption('port')) {

            return $this->input->getOption('host');

        } else {

            return $this->env->get('app.url');
        }
    }

    /**
     * Get the port for the command.
     *
     * @return string
     */
    protected function port() {

        //return $this->input->getOption('port');

        if($this->input->getOption('port')) {

            return $this->input->getOption('port');

        } else {

            return $this->env->get('app.port');
        }
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        return [
            //

        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        return [
            [
                'host',
                null,
                InputOption::VALUE_OPTIONAL,
                'The host address to serve the application on',
                ''
            ],
            [
                'port',
                null,
                InputOption::VALUE_OPTIONAL,
                'The port to serve the application on',
                ''
            ]
        ];
    }

    public function defHost() {

        $defHost = $this->c->get(Config::class)->get('app.url');

        return $defHost;
    }

    protected function defPort() {

        return $defHost = $this->c->get(Config::class)->get('app.port');
    }
}
