<?php

namespace app\console\commands\monitor\tasks;

use app\console\commands\monitor\{
    scheduler\Task
};

use app\events\monitor\{
    EndpointIsDown,
    EndpointIsUp
};

use app\models\{
    monitor\Endpoint
};

use GuzzleHttp\{
    Client,
    Exception\GuzzleException,
    Exception\RequestException
};

use Noodlehaus\Config;

use Symfony\Component\{
    EventDispatcher\EventDispatcher
};

class PingEndpoint extends Task {

    protected $client;
    protected $config;
    protected $dispatcher;
    protected $endpoint;

    /**
     * PingEndpoint constructor.
     *
     * @param Client $client
     * @param Config $config
     * @param EventDispatcher $dispatcher
     * @param Endpoint $endpoint
     */
    public function __construct(Client $client, Config $config, EventDispatcher $dispatcher, Endpoint $endpoint) {

        $this->client = $client;
        $this->config = $config;
        $this->dispatcher = $dispatcher;
        $this->endpoint = $endpoint;
    }

    /**
     * @return mixed|void
     */
    public function handle() {

        try {

            $response = $this->client->request('GET', $this->endpoint->uri, [
                //'verify' => false
            ]);

        } catch (RequestException $e) {

            $response = $e->getResponse();

        } catch (GuzzleException $e) {

            dump($e);

        }

        $this->endpoint->statuses()->create([

            'status_code' => $response->getStatusCode()
        ]);

        $this->dispatchEvents($this->config);
    }

    /**
     * @param $config
     */
    private function dispatchEvents($config) {

        if ($this->endpoint->status->isDown()) {
            $this->dispatcher->dispatch((object)EndpointIsDown::NAME, new EndpointIsDown($config, $this->endpoint));
        }

        if ($this->endpoint->isBackUp()) {
            $this->dispatcher->dispatch((object)EndpointIsUp::NAME, new EndpointIsUp($config, $this->endpoint));
        }
    }
}
