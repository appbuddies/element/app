<?php

namespace app\console\commands\monitor;

use Symfony\Component\Console\{
    Input\ArrayInput,
    Input\InputOption,
    Input\InputArgument,
    Input\InputInterface,
    Output\OutputInterface
};

use app\{
    console\Commands,
    console\traits\CanForce,
    models\monitor\Endpoint
};

class AddEndpointCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'endpoint:add';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Adds an endpoint to monitor';

    /**
     * Handle the command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $endpoint = Endpoint::with([])->where('uri', '=', $input->getArgument('endpoint'))->first();

        if ($endpoint) {

            $output->writeln("<info>Endpoint is already being monitored!</info>");

        } else {

            Endpoint::with([])->create([
                'uri' => $uri = $input->getArgument('endpoint'),
                'frequency' => $input->getOption('frequency')
            ]);

            $output->writeln("<info>Endpoint {$uri} is now being monitored.</info>");
        }

        $this->getApplication()->find('endpoint:status')->run(
            new ArrayInput([
                'command' => 'endpoint:status'
            ]),
            $output
        );
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            [
                'endpoint',
                InputArgument::REQUIRED,
                'The endpoint to monitor'
            ]
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            [
                'frequency',
                'f',
                InputOption::VALUE_OPTIONAL,
                'The frequency to check this endpoint, in minutes.',
                1
            ]
        ];
    }
}