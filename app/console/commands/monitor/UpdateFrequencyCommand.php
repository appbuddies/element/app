<?php

namespace app\console\commands\monitor;

use Symfony\Component\Console\{
    Input\ArrayInput,
    Input\InputOption,
    Input\InputArgument,
    Input\InputInterface,
    Output\OutputInterface
};

use app\{
    console\Commands,
    models\monitor\Endpoint
};

class UpdateFrequencyCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'endpoint:update';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Update the frequency on a specific endpoint';


    /**
     * Handle the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $endpoint = Endpoint::with([])->find($id = $input->getArgument('id'));

        if (!$endpoint) {

            $output->writeln("<error>Endpoint with ID: {$id} does not exist.</error>");
        }

        $endpoint->update([
            'frequency' => $input->getOption('frequency')
        ]);

        $output->writeln("<info>Endpoint {$id} successfully updated!.</info>");

        $this->getApplication()->find('endpoint:status')->run(
            new ArrayInput([
                'command' => 'endpoint:status'
            ]),
            $output
        );
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            [
                'id',
                InputArgument::REQUIRED,
                'The endpoint ID to remove'
            ]
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            [
                'frequency',
                'f',
                InputOption::VALUE_REQUIRED,
                'The frequency to check this endpoint, in minutes.',
                1
            ]
        ];
    }
}