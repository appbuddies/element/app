<?php

namespace app\console\commands\monitor;

use Symfony\Component\{
    Console\Input\InputOption,
    Console\Input\InputArgument,
    Console\Input\InputInterface,
    Console\Output\OutputInterface,
    EventDispatcher\EventDispatcher
};

use app\console\{
    Commands,
    commands\monitor\scheduler\Kernel,
    commands\monitor\tasks\PingEndpoint,
    traits\CanForce
};

use app\models\{
    monitor\Endpoint
};

class Run extends Commands {

    use CanForce;

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'endpoint:run';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return void
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $kernel = new Kernel();

        $endpoints = Endpoint::with([])->get();

        foreach ($endpoints as $endpoint) {

            $kernel
                ->add(new PingEndpoint($this->client, $this->env, $this->dispatcher, $endpoint))
                ->everyMinutes($this->isForced($input) ? 1 : $endpoint->frequency);
        }

        $kernel->run();
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            //
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            [
                'force',
                'f',
                InputOption::VALUE_OPTIONAL,
                'Force check regardless of frequency',
                false
            ],
        ];
    }
}
