<?php

namespace app\console\commands\users;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use app\console\Commands;

use app\models\data\User;

class ShowUsersCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'user:show';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Shows all the users.';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return void
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $user = User::all();

        if($user) {

            foreach ($user as $person) {

                $output->writeln("\n| id: <info>{$person->id}</info>\n|----------------------------------|\n| initials: <info>{$person->initials}</info>\n| email:    <info>{$person->email}</info>\n| role:     <info>{$person->role}</info>\n| active?:  <info>{$person->isActivated}</info>\n|----------------------------------|");
            }

        } else {

            $output->writeln("<error>No users exists in database!?</error>");
        }
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            //
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            //
        ];
    }
}
