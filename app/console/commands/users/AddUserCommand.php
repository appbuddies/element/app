<?php

namespace app\console\commands\users;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use app\console\Commands;

use app\models\data\User;

class AddUserCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'user:add';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Add user to database.';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     * @param User $user
     *
     * @return void
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $user = User::where('email', $this->argument('email'))->first();

        if($user) {

            $output->writeln("<error>{$this->argument('email')} already exists in database!</error>");

        } else {

            $user = User::firstOrCreate([

                'email'     => $this->argument('email')

            ], [

                'password'  => password_hash($this->argument('password'), PASSWORD_DEFAULT),
                'initials'  => $this->argument('initials'),
                'email'     => $this->argument('email'),
            ]);

            $output->writeln("<info>{$this->argument('email')} added to database</info>");
        }
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            ['initials', InputArgument::REQUIRED, 'User initials'],
            ['email', InputArgument::REQUIRED, 'User email'],
            ['password', InputArgument::REQUIRED, 'User password'],
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            //
        ];
    }
}
