<?php

namespace app\console\commands\migrations;

use Phinx\Console\PhinxApplication;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use app\console\Commands;

class Migrate extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'data:migrate-tables';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Migrate the database';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return void
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $phinx = new PhinxApplication();
        $command = $phinx->find('migrate');

        $arguments = [
            'command' => 'migrate'
        ];

        $greetInput = new ArrayInput($arguments);

        try {

            $command->run($greetInput, $output);

        } catch (\Exception $e) {

            dump($e);

        }
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            // ...
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            [
                'environment',
                'e',
                InputOption::VALUE_OPTIONAL,
                'The target environment',
                'development'
            ],
            [
                'target',
                't',
                InputOption::VALUE_OPTIONAL,
                'The version number to migrate to',
                ''
            ],
            [
                'date',
                'd',
                InputOption::VALUE_OPTIONAL,
                'The date to migrate to',
                ''
            ],
            [
                'dry-run',
                'x',
                InputOption::VALUE_OPTIONAL,
                'Dump query to standard output instead of executing it',
                ''
            ],
            [
                'fake',
                null,
                InputOption::VALUE_OPTIONAL,
                "Mark any migrations selected as run, but don't actually execute them",
                ''
            ]
        ];
    }
}
