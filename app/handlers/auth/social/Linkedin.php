<?php

namespace app\handlers\auth\social;

use GuzzleHttp\Exception\GuzzleException;

class Linkedin extends Service {

    /**
     * @return string
     */
    public function getAuthorizeUrl() {

        try {

            return "https://www.linkedin.com/oauth/v2/authorization"
                . "?response_type=code"
                . "&client_id=" . $this->config->get('sso.li.client_id')
                . "&redirect_uri=" . $this->config->get('sso.li.redirect_uri')
                . "&scope=r_liteprofile%20r_emailaddress"
                . "&state=" . bin2hex(random_bytes(32));

        } catch (\Exception $e) {

            return dump($e);
        }
    }

    /**
     * @param $code
     *
     * @return object
     *
     * @throws GuzzleException
     */
    public function getUserByCode($code) {

        $token = $this->getAccessTokenFromCode($code);

        return $this->normalizeUser($this->getUserByToken($token));
    }

    /**
     * @param $code
     *
     * @return mixed
     *
     * @throws GuzzleException
     */
    protected function getAccessTokenFromCode($code) {

        $response = $this->client->request('POST', 'https://www.linkedin.com/oauth/v2/accessToken', [
            'headers' => [
                'Content-Type' => 'x-www-form-urlencoded',
            ],
            'query' => [
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => $this->config->get('sso.li.redirect_uri'),
                'client_id' => $this->config->get('sso.li.client_id'),
                'client_secret' => $this->config->get('sso.li.client_secret'),
            ]
        ])->getBody();

        return json_decode($response);
    }

    /**
     * @param $token
     *
     * @return mixed
     *
     * @throws GuzzleException
     */
    protected function getUserByToken($token) {

        $response = $this->client->request('GET', 'https://api.linkedin.com/v2/me', [
            'query' => [
                'fields' => 'id,firstName,lastName,profilePicture'
            ],
            'headers' => [
                "Authorization" => "Bearer " . $token->access_token,
                "Content-Type"  => "application/json",
                "x-li-format"   =>"json"
            ],
        ])->getBody();

        return json_decode($response);
    }

    /**
     * @param $user
     *
     * @return object
     */
    protected function normalizeUser($user) {

        return (object) [

            'uid'       => $user->id,
            'username'  => 'TODO',
            'name'      => $user->firstName->localized->da_DK . " " . $user->lastName->localized->da_DK,
            'email'     => 'TODO',
            'photo'     => $user->profilePicture->displayImage,
        ];
    }
}
