<?php

namespace app\handlers\auth\social;

class Google extends Service {

    public function getAuthorizeUrl() {

        try {

            return "https://www.linkedin.com/oauth/v2/authorization"
                . "?response_type=code"
                . "&client_id=" . $this->config->get('sso.li.client_id')
                . "&redirect_uri=" . $this->config->get('sso.li.redirect_uri')
                . "&scope=r_liteprofile%20r_emailaddress"
                . "&state=" . bin2hex(random_bytes(32));

        } catch (\Exception $e) {

            return dump($e);
        }
/*
            'response_type' => 'code',
            'access_type' => 'offline',
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUri,
            'state' => $this->state,
            'scope' => $this->getScope(),
 */
    }

    public function getUserByCode($code) {

        $token = $this->getAccessTokenFromCode($code);

        return $this->normalizeUser($this->getUserByToken($token));
    }

    protected function getAccessTokenFromCode($code) {

        $response = $this->client->request('GET', 'https://graph.facebook.com/v2.3/oauth/access_token', [
            'query' => [
                'client_id' => $this->config->get('sso.fb.client_id'),
                'client_secret' => $this->config->get('sso.fb.client_secret'),
                'redirect_uri' => $this->config->get('sso.fb.redirect_uri'),
                'code' => $code,
            ]
        ])->getBody();

        return json_decode($response)->access_token;
    }

    protected function getUserByToken($token) {

        $response = $this->client->request('GET', 'https://graph.facebook.com/me', [
            'query' => [
                'access_token' => $token,
                'fields' => 'id,name,email,picture'
            ],
        ])->getBody();

        return json_decode($response);
    }

    protected function normalizeUser($user) {

        return (object) [

            'uid'       => $user->id,
            'username'  => $user->id,
            'name'      => $user->name,
            'email'     => $user->email,
            'photo'     => $user->picture->data->url,
        ];
    }
}
