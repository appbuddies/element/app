<?php

namespace app\handlers\auth;

use app\models\data\UserSocial;

class AuthSocial {

	/**
	 * ...
	 */
	public function sso() {

        if(isset($_SESSION['sso'])) {

            return UserSocial::find($_SESSION['sso']);
        }

        return $_SESSION['sso'] = null;
	}

	/**
	 * ...
	 */
	public function check() {

		return isset($_SESSION['sso']);
	}

    /**
     * ...
     *
     * @param $service
     * @param $uid
     *
     * @return bool
     */
    public function store($service, $uid) {

        // Attempt to grab the company by name
        $sso = UserSocial::with([])->where('service', '=', $service)->where('uid', $uid)->first();

        // Check if the company exists or not ?!
        if(!$sso) {

            return false;
        }

        // verify rules for the found customer
        if($sso->service) {

            // set into session
            $_SESSION['sso'] = $sso->id;
            return true;
        }

        return false;
    }

    /**
     * ...
     */
    public function clear() {

        unset($_SESSION['sso']);
    }
}