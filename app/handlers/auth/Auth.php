<?php

namespace app\handlers\auth;

use app\models\data\User;

class Auth {

	/**
	 * ...
	 */
	public function user() {

        if(isset($_SESSION['user'])) {

            return User::with([
                'role',
                'sso',
            ])->find($_SESSION['user']);
        }

        return $_SESSION['user'] = null;
	}

	/**
	 * ...
	 */
	public function check() {

		return isset($_SESSION['user']);
	}

    /**
     * @param $email
     * @param $password
     *
     * @return bool
     */
	public function attempt($email, $password) {

		// Attempt to grab the user by email
		$user = User::with([])->where('email', '=', $email)->first();

		// Check if the customer exists or not ?!
		if(!$user) {

			return false;
		}

		// verify rules for the found customer
		if(password_verify($password, $user->password)) {

            // set USER into session
            $_SESSION['user'] = $user->id;

            $this->setOnlineStatus($_SESSION['user']);

            return true;
		}

		return false;
	}

    /**
     * ...
     */
    public function activate($token) {

        // Attempt to grab the user by email
        $user = User::with([])->where('id', '=', $_SESSION['user'])->first();

        // Check if the customer exists or not ?!
        if(!$user) {

            return false;
        }

        // verify rules for the found customer
        if($token == $user->token) {

            User::with([])->where('id', '=', $user->id)->update([

                'token'         => null,
                'isActivated'   => 1
            ]);

            return true;
        }

        return false;
    }

	/**
	 * ...
	 */
	public function logout() {

	    $this->setOfflineStatus($_SESSION['user']);

		unset($_SESSION['user']);
	}

    /**
     * @param $id
     */
    private function setOnlineStatus($id) {

	    User::with([])->where('id', '=', $id)->update([

	        'status' => 1
        ]);
    }

    /**
     * @param $id
     */
    private function setOfflineStatus($id) {

        User::with([])->where('id', '=', $id)->update([

            'status' => 3
        ]);
    }
}