<?php

namespace app\middlewares;

class BaseMiddleware {

    protected $c;

    /**
     * BaseMiddleware constructor.
     * @param $container
     */
    public function __construct($container) {

        $this->c = $container;
    }
}