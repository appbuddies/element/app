<?php

namespace app\middlewares\front;

use app\handlers\auth\Auth;
use Slim\Router;

class AuthMiddleware {

	protected $auth;
	private $router;

	/**
	 * AuthMiddleware constructor.
	 *
	 * @param Auth $auth
	 * @param Router $router
	 */
	public function __construct(Auth $auth, Router $router) {

		$this->auth = $auth;
		$this->router = $router;
	}

	/**
	 * @param $request
	 * @param $response
	 * @param $next
	 *
	 * @return mixed
	 */
	public function __invoke($request, $response, $next) {

        /**
         * Redirect to signin IF the user is activated
         */
		if (!$this->auth->check()) {

            return $response->withRedirect($this->router->pathFor('home'));
		}

		$response = $next($request, $response);
		return $response;
	}
}