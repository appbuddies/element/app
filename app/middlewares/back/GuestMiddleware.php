<?php

namespace app\middlewares\back;

use app\handlers\auth\Auth;
use Slim\Router;

class GuestMiddleware {

	protected $auth;
    protected $router;

    /**
     * AuthMiddleware constructor.
     *
     * @param Auth $auth
     * @param Router $router
     */
	public function __construct(Auth $auth, Router $router) {

		$this->auth = $auth;
		$this->router = $router;
	}

	/**
	 * @param $request
	 * @param $response
	 * @param $next
	 *
	 * @return mixed
	 */
	public function __invoke($request, $response, $next) {

		if ($this->auth->check()) {

			//$this->flash->addMessage('success', "You're already signed in!");
			return $response->withRedirect($this->router->pathFor('dashboard'));
		}

		$response = $next($request, $response);
		return $response;
	}
}