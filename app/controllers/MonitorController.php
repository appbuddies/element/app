<?php

namespace app\controllers;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class MonitorController extends BaseController {

    /**
     * @param Response $response
     *
     * @return Response
     */
    public function getMonitorEndpoints(Response $response) {

        return $this->view->render($response, '/admin/MonitorEndpoints.twig', [

            // TODO
        ]);
    }

    public function postMonitorEndpoints() {

        // TODO
    }

    /**
     * @param Response $response
     *
     * @return mixed
     */
    public function cronRunUptime(Response $response) {

        system('php element endpoint:run');

        return $response->withRedirect($this->router->pathFor('admin.back'));
    }
}
