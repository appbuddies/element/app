<?php

namespace app\controllers;

use app\models\{
    data\User,
    upload\Image
};

use Illuminate\Pagination\LengthAwarePaginator;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use Slim\App;

class HomeController extends BaseController {

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function home(Request $request, Response $response, User $user) {

        $users      = $user->with([])->get()->all();

        $page       = $request->getParam('page', 1);
        $perPage    = $request->getParam('perPage', 3);

        $userPaginator  = new LengthAwarePaginator(
            array_slice($users, ($page - 1) * $perPage, $perPage),
            count($users),
            $perPage,
            $page,
            [
                'path'  => $request->getUri()->getPath(),
                'query' => $request->getParams()
            ]
        );

        return $this->view->render($response, '/front/Home.twig', [

            'users' => $users,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function sales(Request $request, Response $response, User $user) {

        $users = $user->with([])->get();

        return $this->view->render($response, '/back/dashboards/Sales.twig', [

            'users'     => $users,
        ]);
    }

    /**
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function analytics(Response $response, User $user) {

        $users = $user->with([])->get();

        return $this->view->render($response, '/back/dashboards/Analytics.twig', [

            'users' => $users,
        ]);
    }

    /**
     * @param Response $response
     *
     * @return mixed
     */
    public function backupDatabase(Response $response) {

        exec('mysqldump --host='.$this->config->get('db.mysql.host').' --port='.$this->config->get('db.mysql.port').' --user='.$this->config->get('db.mysql.user').' --password='.$this->config->get('db.mysql.password').' '.$this->config->get('db.mysql.database').' > ./storage/database/data_'.date("d-m-Y_H:i:s").'.sql');

        return $response->withRedirect($this->router->pathFor('back.sales'));
    }
}