<?php

namespace app\controllers;

use app\handlers\auth\{
    AuthSocial,
    social\Facebook,
    social\Github,
    social\Google,
    social\Linkedin,
    social\Twitter
};

use app\models\{
    data\UserSocial
};

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use Respect\Validation\Validator as v;

class AuthSocialController extends BaseController {

    /**
     * sign -> IN -> WITH FACEBOOK
     *
     * @param Facebook $auth
     */
    public function getFacebookSignIn(Facebook $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    /**
     * @param Response $response
     * @param Facebook $auth
     * @param AuthSocial $sso
     *
     * @return void
     */
    public function getFacebookStatus(Response $response, Facebook $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {

            $this->flash->addMessage('danger', 'something went wrong?! Please try again...');
            return $response->withRedirect($this->router->pathFor('home.signin'));
        }

        $user = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'facebook',
            'uid'       => $user->uid

        ], [

            'service'   => 'facebook',
            'uid'       => $user->uid,
            'name'      => $user->name,
            'email'     => $user->email,
            'photo'     => $user->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id == null) {

            return $response->withRedirect($this->router->pathFor('home.signin.link-accounts'));
        }
    }

    /**
     * sign -> IN -> WITH GITHUB
     *
     * @param Github $auth
     */
    public function getGithubSignIn(Github $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    /**
     * @param Response $response
     * @param Github $auth
     * @param AuthSocial $sso
     *
     * @return mixed
     */
    public function getGithubStatus(Response $response, Github $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {
            die();
        }

        $user = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'github',
            'email'     => $user->email

        ], [

            'service'   => 'github',
            'uid'       => $user->uid,
            'username'  => $user->username,
            'name'      => $user->name,
            'email'     => $user->email,
            'photo'     => $user->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id == null) {

            return $response->withRedirect($this->router->pathFor('home.signin.link-accounts'));
        }
    }

    /**
     * sign -> IN -> WITH GOOGLE
     *
     * @param Google $auth
     */
    public function getGoogleSignIn(Google $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    public function getGoogleStatus(Google $auth, Response $response) {

        if (!isset($_GET['code'])) {

            $this->flash->addMessage('danger', 'something went wrong?! Please try again...');
            return $response->withRedirect($this->router->pathFor('home.signin'));
        }

        $user = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'facebook',
            'uid'       => $user->email

        ], [

            'service'   => 'facebook',
            'uid'       => $user->uid,
            'name'      => $user->name,
            'email'     => $user->email,
            'photo'     => $user->photo,
        ]);

        if ($user->user_id == null) {

            return $response->withRedirect($this->router->pathFor('home.signin.link-accounts'));
        }
    }

    /**
     * sign -> IN -> WITH LINKEDIN
     *
     * @param Linkedin $auth
     */
    public function getLinkedinSignIn(Linkedin $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    public function getLinkedinStatus(Response $response, Linkedin $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {

            $this->flash->addMessage('danger', 'something went wrong?! Please try again...');
            return $response->withRedirect($this->router->pathFor('home.signin'));
        }

        $user = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'linkedin',
            'uid'       => $user->uid

        ], [

            'service'   => 'linkedin',
            'uid'       => $user->uid,
            'name'      => $user->name,
            'email'     => $user->email,
            'photo'     => $user->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id == null) {

            return $response->withRedirect($this->router->pathFor('home.signin.link-accounts'));
        }
    }

    /**
     * sign -> IN -> WITH TWITTER
     *
     * @param Twitter $auth
     */
    public function getTwitterSignIn(Twitter $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    public function getTwitterStatus(Twitter $auth, Response $response) {

        if (!isset($_GET['code'])) {
            die();
        }

        $user = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'twitter',
            'uid'       => $user->email

        ], [

            'service'   => 'twitter',
            'uid'       => $user->uid,
            'name'      => $user->name,
            'email'     => $user->email,
            'photo'     => $user->photo,
        ]);

        if ($user->user_id == null) {

            return $response->withRedirect($this->router->pathFor('home.signin.link-accounts'));
        }
    }
}