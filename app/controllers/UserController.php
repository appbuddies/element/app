<?php

namespace app\controllers;

use app\models\{
    data\User,
    data\UserAddresses,
    data\UserContact,
    data\UserSocial,
    mail\NewToken,
    mail\Welcome
};

use app\utils\{
    TokenGenerator
};

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use Respect\Validation\Validator as v;

class UserController extends BaseController {

    /**
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function getUsers(Response $response, User $user) {

        $users = $user->with(['role'])->get();

        return $this->view->render($response, '/back/users/Users.twig', [

            'users' => $users,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     *
     * @return mixed
     */
    public function postUser(Request $request, Response $response, User $user) {

        /**
         * doing some basic validation BEFORE signup is executed
         */
        $validation = $this->validator->validate($request, [

            'Email'             => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'Password'          => v::noWhitespace()->notEmpty()->length(6, null),
            'Confirm-Password'  => v::noWhitespace()->notEmpty()->confirmPassword($request->getParam('Password'))
        ]);

        /**
         * if validation fails, then we redirect the user to : /BACK/USER/CATALOGUE
         */
        if ($validation->fails()) {

            return $response->withRedirect($this->router->pathFor('user.catalogue'));
        }

        $generate = new TokenGenerator();
        $token = $generate->codeGenerate();

        /**
         * if validation is acceptable, THEN signup is being executed
         */
        $user = User::with([])->create([

            'email'         => $request->getParam('Email'),
            'password'      => password_hash($request->getParam('Password'), PASSWORD_DEFAULT),
            'role'          => 1,
            'first_name'    => ucwords($request->getParam('First-Name')),
            'last_name'     => ucwords($request->getParam('Last-Name')),
            'token'         => $token,
            'isActivated'   => false
        ]);

        $newUser = User::with([])->orderBy('created_at', 'desc')->first();

        /**
         * after the user is created, we then go ahead and create his contacts (db relation)
         */
        if ($newUser) {

            UserContact::with([])->create([

                'user_id'       => $newUser->id,
                'tel_home'      => '...',
                'tel_mobile'    => '...',
                'tel_work'      => '...',
                'accepts_sms'   => false
            ]);
        }

        /**
         * Show a toast after signup is successfully finished
         */
        $this->flash->addMessage('success', 'Welcome to Element');

        /**
         * AND send a welcome-mail to the user, that we've just created the profile!...
         */
        $user        = new User;
        $user->name  = ucwords( $request->getParam('First-Name'));
        $user->email = $request->getParam('Email');
        $user->token = $token;

        $this->mailer->to($user->email, $user->name)->send(new Welcome($user, $this->translator));

        return $response->withRedirect($this->router->pathFor('back.sales'));
    }

    /**
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function getUserAccount(Response $response, User $user) {

        $user = $user->with([

            //... insert relations here

        ])->where('id', '=', $this->auth->user()->id)->first();

        if (!$this->auth->user()->id) {

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }

        return $this->view->render($response, 'back/users/UserProfile.twig', [

            'user' => $user,
        ]);
    }

    /**
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function getUserInbox(Response $response, User $user) {

        $user = $user->with([

            //... insert relations here

        ])->where('id', '=', $this->auth->user()->id)->first();

        if (!$this->auth->user()->id) {

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }

        return $this->view->render($response, 'back/users/UserInbox.twig', [

            'user' => $user,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     */
    public function postUserInbox(Request $request, Response $response, User $user) {

        dump("Email sent");
        die;
    }

    /**
     * @param Response $response
     *
     * @return Response
     */
    public function getUserActivation(Response $response) {

        return $this->view->render($response, 'back/users/ActivateAccount.twig', [

            //
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function activateUser(Request $request, Response $response) {

        /**
         * attempt to signin...
         */
        $activate = $this->auth->activate(

            $request->getParam('token')
        );

        /**
         * if signin FAILS, then we redirect back...
         */
        if (!$activate) {

            $this->flash->addMessage('danger', 'Hmm ?! Prøv igen...');

            return $response->withRedirect($this->router->pathFor('auth.activate.user'));
        }

        $this->flash->addMessage('success', 'velkommen');

        return $response->withRedirect($this->router->pathFor('back.sales'));
    }

    /**
     * @param Response $response
     *
     * @return mixed
     */
    public function resendActivationEmail(Response $response) {

        $generate = new TokenGenerator();
        $token = $generate->codeGenerate();

        $this->auth->user()->updateActivationToken($token);

        $user        = new User;
        $user->name  = $this->auth->user()->first_name ." ". $this->auth->user()->last_name;
        $user->email = $this->auth->user()->email;
        $user->token = $token;

        $this->mailer->to($user->email, $user->name)->send(new NewToken($user, $this->translator));

        $this->flash->addMessage('success', 'SUCCESS!');

        return $response->withRedirect($this->router->pathFor('auth.activate.user'));
    }
}