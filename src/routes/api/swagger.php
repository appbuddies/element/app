<?php

/**
 * OUTER Group that DOESN'T applies CSRF to routes
 */
$app->group('/api', function () use($app, $container) {

    /**
     * SWAGGER UI
     */
    $app->group('/v1', function () use($app, $container) {

        /**
         * rendering view : API -> SWAGGER-UI
         */
        $this->get('/welcome', ['api\controllers\SwaggerController', 'getApiVersion1Ui'])->setName('api.v1.get.swagger-ui');

    });

});