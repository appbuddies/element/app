import Vue from 'vue'
import Router from 'vue-router'

// (~) is an alias for ./src/components
import SalesDashboard from '^/back/dashboards/Sales';
import AnalyticsDashboard from '^/back/dashboards/Analytics';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/back/sales',
            name: 'back.sales',
            component: SalesDashboard,
        },
        {
            path: '/back/analytics',
            name: 'back.analytics',
            component: AnalyticsDashboard,
        }
    ]
})