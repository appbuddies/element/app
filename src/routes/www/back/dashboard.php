<?php

use Slim\{
    Csrf\Guard,
    Views\Twig
};

use app\handlers\auth\Auth;

use app\middlewares\back\{
    AuthMiddleware as AdminAuth,
    GuestMiddleware as AdminGuest
};

use app\middlewares\front\{
    AuthMiddleware as HomeAuth,
    GuestMiddleware as HomeGuest
};

use app\middlewares\{
    CsrfViewMiddleware,
    other\CurrentRouteMiddleware
};

/**
 * OUTER Group that applies CSRF to routes
 */
$app->group('/back', function () use($app, $container) {

    /**
     * Group that DOES require the user to be signed in
     */
    $app->group('', function () use($app, $container) {

        /**
         * rendering view : HOME(back) -> DASHBOARD
         */
        $app->get('/sales', [app\controllers\HomeController::class, 'sales'])->setName('back.sales');

        /**
         * rendering view : HOME(back) -> DASHBOARD
         */
        $app->get('/analytics', [app\controllers\HomeController::class, 'analytics'])->setName('back.analytics');

        /**
         * rendering view : HOME(back) -> USER
         */
        //$this->get('/user/profile', ['app\controllers\AdminController', 'getAdminProfile'])->setName('back.user.profile');
        //$this->post('/user/profile', ['app\controllers\AdminController', 'postAdminProfile']);

        /**
         * action : CLEAR SESSION
         */
        //$this->get('/clear-session', ['app\controllers\SessionController', 'clearSession'])->setName('clear-session');

    })->add(new AdminAuth($container->get(Auth::class), $container->get(\Slim\Router::class)));

    /**
     * Group that DOESN'T require the user to be signed in nor a guest!
     */
    $app->group('', function () {

        /**
         * action : CRON JOB -> BACKUP -> DATABASE
         */
        $this->get('/backup/database', ['app\controllers\HomeController', 'backupDatabase'])->setName('back.backup.database');

    });

})->add(new CsrfViewMiddleware($container->get(Twig::class), $container->get(Guard::class)))
    ->add($container->get(Guard::class))
    ->add(new CurrentRouteMiddleware())
;