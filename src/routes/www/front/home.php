<?php

use Slim\{
    Csrf\Guard,
    Views\Twig
};

use app\handlers\auth\Auth;

use app\middlewares\back\{
    AuthMiddleware as AdminAuth,
    GuestMiddleware as AdminGuest
};

use app\middlewares\front\{
    AuthMiddleware as HomeAuth,
    GuestMiddleware as HomeGuest
};

use app\middlewares\{
    CsrfViewMiddleware,
    other\CurrentRouteMiddleware
};

/**
 * OUTER Group that applies CSRF to routes
 */
$app->group('', function () use($app, $container) {

    $app->get('/', [app\controllers\HomeController::class, 'home'])->setName('home');

})
    ->add(new CsrfViewMiddleware($container->get(Twig::class), $container->get(Guard::class)))
    ->add($container->get(Guard::class))
    ->add(new CurrentRouteMiddleware())
;