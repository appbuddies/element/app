import axios from 'axios';

/**
 *
 * @param commit
 */
export const fetchBahayAuthentication = ({ commit }) => {

    let body = JSON.stringify({
        email: "stefan@korfitz.com",
        password: "iokl+923"
    });

    axios
        .post(`/api/auth/fetch-token`, JSON.parse(body),{
            //...
        })
        .then((response) => {
            commit('saveAuthToken', response.data.token);
        })
        .catch(function (error) {
            console.log(error);
        });
};

export const getCustomers = ({ state, commit }) => {

    /**
     * JWT auth token from Bahay api
     */
    let token = state.auth.token;

    axios
        .get(`/api/get/users`, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
        .then((response) => {
            commit('addCustomer', response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
};

export const getOrders = ({ state, commit }) => {

    /**
     * JWT auth token from Bahay api
     */
    let token = state.auth.token;

    axios
        .get(`/api/get/orders`, {
            headers: {
                Authorization: "Bearer " + token
            }
        })
        .then((response) => {
            commit('addOrder', response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
};

export const getLocale = ({ state, commit }) => {

    /**
     * JWT auth token from Bahay api
     */
    //let token = state.auth.token;

    axios
        .get(`/api/get/admin/locale`, {
            // headers: {
            //     Authorization: "Bearer " + token
            // }
        })
        .then((response) => {
            commit('saveLocale', response.data)
        })
        .catch(function (error) {
            console.log(error);
        });
};
