/**
 *
 * @param state
 * @param payload
 */
export const saveAuthToken = (state, payload) => {
    state.auth.token = payload
};

/**
 *
 * @param state
 * @param payload
 */
export const addCustomer = (state, payload) => {

    payload.forEach(save);

    function save(item) {

        /**
         * ...array of id's of the customers that we've already have in state
         */
        let storedCustomers = state.customers.map(function(a) {return a.id;});

        /**
         * ...check to see if the new id exists in the array of the ones we already have?
         *
         * @type {boolean}
         */
        let lookupCustomers = storedCustomers.includes(item.id);

        /**
         * ...IF we don't have the id of a particular customer already?
         * ...Only THEN we go ahead and commit our mutation!
         */
        lookupCustomers ? console.log("EXISTS ALREADY!") : state.customers.push(item);
    }
};

/**
 *
 * @param state
 * @param payload
 */
export const addOrder = (state, payload) => {

    payload.forEach(save);

    function save(item) {

        /**
         * ...array of id's of the orders that we've already have in state
         */
        let storedOrders = state.orders.map(function(a) {return a.id;});

        /**
         * ...check to see if the new id exists in the array of the ones we already have?
         *
         * @type {boolean}
         */
        let lookupOrders = storedOrders.includes(item.id);

        /**
         * ...IF we don't have the id of a particular order already?
         * ...Only THEN we go ahead and commit our mutation!
         */
        lookupOrders ? console.log("ORDER EXISTS ALREADY!") : state.orders.push(item);
    }
};

/**
 *
 * @param state
 * @param payload
 */
export const saveLocale = (state, payload) => {
    state.locale.push(payload)
};
