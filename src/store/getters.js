/**
 *
 * @param state
 * @returns {void|*|IDBRequest<number>}
 */
export const authenticate = (state) => {
    return state.auth
};

/**
 *
 * @param state
 * @returns {void|*|IDBRequest<number>}
 */
export const customers = (state) => {
    return state.customers
};

/**
 *
 * @param state
 * @returns {void|*|IDBRequest<number>}
 */
export const orders = (state) => {
    return state.orders
};

/**
 *
 * @param state
 * @returns {void|*|IDBRequest<number>}
 */
export const locale = (state) => {
    return state.locale
};
