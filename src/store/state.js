export default {
    auth : {
        token: ""
    },
    orders : [],
    turnover : {
        annual: null,
        total:  null
    },
    customers : [],
    locale : [],
}
