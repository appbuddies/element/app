import Vue from 'vue';
import App from '^/BackView';
import bootstrap from 'bootstrap-vue';
import router from '#/www/back/vue-router';
import store from '&/store';

Vue.config.productionTip = true;

new Vue({
    el: '#app',
    bootstrap,
    router,
    store,
    render: h => h(App)
});
