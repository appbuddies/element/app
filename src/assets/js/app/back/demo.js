$(function () {
    "use strict";
    // ==============================================================
    // world map
    // ==============================================================
    jQuery('#visitfromworld').vectorMap({
        map: 'world_mill_en'
        , backgroundColor: '#fff'
        , borderColor: '#ccc'
        , borderOpacity: 0.9
        , borderWidth: 1
        , zoomOnScroll : false
        , color: '#ddd'
        , regionStyle: {
            initial: {
                fill: '#fff'
            }
        }
        , markerStyle: {
            initial: {
                r: 8
                , 'fill': '#55ce63'
                , 'fill-opacity': 1
                , 'stroke': '#000'
                , 'stroke-width': 0
                , 'stroke-opacity': 1
            }
            , }
        , enableZoom: true
        , hoverColor: '#79e580'
        , markers: [{
            latLng: [21.00, 78.00]
            , name: 'India : 9347'
            , style: {fill: '#55ce63'}
        },
            {
                latLng : [-33.00, 151.00],
                name : 'Australia : 250'
                , style: {fill: '#02b0c3'}
            },
            {
                latLng : [36.77, -119.41],
                name : 'USA : 250'
                , style: {fill: '#11a0f8'}
            },
            {
                latLng : [55.37, -3.41],
                name : 'UK   : 250'
                , style: {fill: '#745af2'}
            },
            {
                latLng : [25.20, 55.27],
                name : 'UAE : 250'
                , style: {fill: '#ffbc34'}
            }]
        , hoverOpacity: null
        , normalizeFunction: 'linear'
        , scaleColors: ['#fff', '#ccc']
        , selectedColor: '#c9dfaf'
        , selectedRegions: []
        , showTooltip: true
        , onRegionClick: function (element, code, region) {
            var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
            alert(message);
        }
    });
    // ############################################################
    // Sales of the Month
    // ############################################################
    var chart = c3.generate({
        bindto: '#visitor',
        data: {
            columns: [
                ['Item A', 9],
                ['Item B', 3],
                ['Item C', 2],
                ['Item D', 2],
            ],

            type: 'donut',
            tooltip: {
                show: true
            }
        },
        donut: {
            label: {
                show: false
            },
            title: "",
            width: 15,
        },

        legend: {
            hide: true
        },
        color: {
            pattern: ['#edf1f5', '#009efb', '#55ce63', '#745af2']
        }
    });
    // ############################################################
    // Revenue Statistics
    // ############################################################
    new Chartist.Line('.revenue', {
        labels: ['0', '4', '8', '12', '16', '20', '24', '30']
        , series: [
            [0, 2, 3.5, 0, 13, 1, 4, 1]
            , [0, 4, 0, 4, 0, 4, 0, 4]
        ]
    }, {
        high: 15
        , low: 0
        , showArea: true
        , fullWidth: true
        , plugins: [
            Chartist.plugins.tooltip()
        ],
        // As this is axis specific we need to tell Chartist to use whole numbers only on the concerned axis
        axisY: {
            onlyInteger: true
            , offset: 20
            , labelInterpolationFnc: function (value) {
                return (value / 1) + 'k';
            }
        }
    });

    // ############################################################
    // Sales difference
    // ############################################################
    new Chartist.Pie('.sales-diff', {
        series: [35, 15, 10]
    }, {
        donut: true
        , donutWidth: 20
        , startAngle: 0
        , showLabel: false
    });
});


// ############################################################
// Sales Prediction
// ############################################################
var gaugeChart = echarts.init(document.getElementById('sales-prediction'));
option = {
    tooltip: {
        formatter: "{a} <br/>{b} : {c}%"
    }
    , toolbox: {
        show: false
        , feature: {
            mark: {
                show: true
            }
            , restore: {
                show: true
            }
            , saveAsImage: {
                show: true
            }
        }
    }
    , series: [
        {
            name: 'Sales'
            , type: 'gauge'
            , splitNumber: 1,
            axisLine: {
                lineStyle: {
                    color: [[0.2, '#029ff6'], [0.8, '#1badcb'], [1, '#42c386']]
                    , width: 20
                }
            }
            , axisTick: {
                splitNumber: 0,
                length: 12,
                lineStyle: {
                    color: 'auto'
                }
            }
            , axisLabel: {
                textStyle: {
                    color: 'rgba(255,255,255,0.025)'
                }
            }
            , splitLine: {
                show: false,
                length: 50,
                lineStyle: {
                    color: 'auto'
                }
            }
            , pointer: {
                width: 5
                , color: '#54667a'
            }
            , title: {
                show: false
                , offsetCenter: [0, '-40%'],
                textStyle: {
                    fontWeight: 'bolder'
                }
            }
            , detail: {
                textStyle: {
                    color: 'auto'
                    , fontSize: '12'
                    , fontWeight: 'bolder'
                }
            }
            , data: [{
                value: 50
                , name: 'Sales'
            }]
        }
    ]
};
timeTicket = setInterval(function () {
    option.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;
    gaugeChart.setOption(option, true);
}, 2000)
// use configuration item and data specified to show chart
gaugeChart.setOption(option, true), $(function () {
    function resize() {
        setTimeout(function () {
            gaugeChart.resize()
        }, 100)
    }

    $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

