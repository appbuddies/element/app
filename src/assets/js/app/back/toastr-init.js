$(function() {

    // CREATE USER -> SUCCESS
    $('#backupDB').on('click', function() {
        toastr.success('Database copied!', "look in (/storage/database) folder", { "progressBar": true });
    });

    // CREATE USER -> SUCCESS
    $('#create-user').on('click', function() {
        toastr.success('New user submitted!', 'User created', { "progressBar": true });
    });

});