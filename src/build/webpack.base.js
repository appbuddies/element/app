const path = require('path');

const webpack = require('webpack');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {

    devtool: 'source-map',

    mode: 'none',

    entry: {

        'api/app'                   : './src/assets/js/api/app.js',

        'back/apex-charts'          : './src/assets/js/app/back/apex-charts.js',
        'back/app'                  : './src/assets/js/app/back/app.js',
        'back/app-init'             : './src/assets/js/app/back/app-init.js',
        'back/app-sidebar'          : './src/assets/js/app/back/app-sidebar.js',
        'back/app-style-switcher'   : './src/assets/js/app/back/app-style-switcher.js',
        'back/custom'               : './src/assets/js/app/back/custom.js',
        'back/dashboard'            : './src/assets/js/app/back/dashboard.js',
        'back/demo'                 : './src/assets/js/app/back/demo.js',
        'back/email'                : './src/assets/js/app/back/email.js',
        'back/feather'              : './src/assets/js/app/back/feather.js',
        'back/sidebar-menu'         : './src/assets/js/app/back/sidebar-menu.js',
        'back/sparkline'            : './src/assets/js/app/back/sparkline.js',
        //'back/sweetalert2'          : './src/assets/js/app/back/sweetalert2.js',
        //'back/sweetalert2-init'     : './src/assets/js/app/back/sweetalert2-init.js',
        'back/toastr-init'          : './src/assets/js/app/back/toastr-init.js',
        'back/vue'                  : './src/assets/js/vue/back.js',
        'back/waves'                : './src/assets/js/app/back/waves.js',

        'front/app'                 : './src/assets/js/app/front/app.js',
        'front/custom'              : './src/assets/js/app/front/custom.js',
    },

    output: {
        filename: 'js/[name].min.js',
        path: path.resolve(__dirname, '../../public')
    },

    resolve: {
        alias: {
            '@': path.resolve('./src'),
            '^': path.resolve('./src/views'),
            '~': path.resolve('./src/components'),
            '#': path.resolve('./src/routes'),
            '&': path.resolve('./src/store'),
            'fonts': path.resolve('./src/assets/fonts'),
            'icons': path.resolve('./src/assets/icons'),
            'images': path.resolve('./src/assets/home'),
            'js': path.resolve('./src/assets/js'),
            'scss': path.resolve('./src/assets/scss'),
            'public': path.resolve('./public'),
        },
        extensions: [
            '.js',
            '.vue',
            '.scss',
            '.twig'
        ]
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader'
                }]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: true // TODO
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                })
            },
            // {
            //     test: /\.(png|jpg|gif|svg|txt)$/,
            //     use: [
            //         {
            //             loader: 'file-loader',
            //             options: {
            //                 name(robots) {
            //                     return '[path][name].[ext]';
            //                 },
            //             },
            //         },
            //     ],
            // },
        ]
    },

    plugins: [

        new ExtractTextPlugin({
            filename:  (getPath) => {
                return getPath('css/[name].min.css').replace('css', 'css');
            },
            allChunks: true
        }),

        new OptimizeCSSPlugin({
            cssProcessorOptions: {
                map: {
                    inline: false
                },
                discardComments: {
                    removeAll: false
                }
            }
        }),

        new HtmlWebpackPlugin({
            filename: 'welcome.html',
            template: path.join(__dirname, './static/welcome.html'),
            files: {
                "css": [
                    "css/[name].min.css"
                ],
                "js": [
                    "js/[name].min.js"
                ],
            }
        }),

        // new HtmlWebpackPlugin({
        //     filename: 'robots.txt',
        //     template: path.join(__dirname, './../../public/robots.txt'),
        // }),
        //
        // new HtmlWebpackPlugin({
        //     filename: 'sitemap.xml',
        //     template: path.join(__dirname, './../../public/sitemap.xml'),
        // }),

        new VueLoaderPlugin(),

        new WebpackNotifierPlugin({
            contentImage: path.join(__dirname, './static/icons/element.png'),
            title: 'Elements collected',
            sound: true, // true | false.

        }),

    ]

};
