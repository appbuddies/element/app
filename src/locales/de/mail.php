<?php
/**
 * lang? = Danish (_da)
 */
return [

    /*--------------------------------------------------|
    |                                                   |
    |   MAIL -> USER -> WELCOME                         |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * auth -> new token (_auth)
     */
    'auth.welcome_title'        => "Here is your new token 📣 🔐",
    'auth.welcome_greet'        => "Hi",
    'auth.welcome_heading'      => "We are sending you this email, because you've recently created a profile",
    'auth.welcome_subtitle'     => "Before you can login to [INSERT APP NAME], we need you to activate your account ✔️ 😄",
    'auth.welcome_instructions' => "In order for you to do that, you will need \n your activation-key, which is this one 👉",
    'auth.welcome_cta_button'   => "Activate",
    'auth.welcome_regards'      => "- This is an automated response",
    'auth.welcome_sender'       => "from an Element 🤖 named Bob",

    /*--------------------------------------------------|
    |                                                   |
    |   MAIL -> USER -> NEW TOKEN                       |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * auth -> new token (_auth)
     */
    'auth.new_token_title'      => "Here is your new token 📣 🔐",
    'auth.new_token_greet'      => "Hi",
    'auth.new_token_heading'    => "Lost your activation-token hey?!...",
    'auth.new_token_dont_worry' => "Don't worry mate ✔️ 😄",
    'auth.new_token_generated'  => "We've gone ahead and made you a new one 👉",
    'auth.new_token_regards'    => "- This is an automated response",
    'auth.new_token_sender'     => "from an Element 🤖 named Bob",
];
