<?php
/**
 * lang? = Danish (_da)
 */
return [

    /**
     * app -> title & meta (_app)
     */
    'app.admin_default_title'   => "Nexus | Admin",
    'app.authentication_login'  => "Nexus | Login",
    'app.authentication_signup' => "Nexus | Signup",
    'app.dashboard_sales'       => "Nexus | Sales",
    'app.dashboard_analytics'   => "Nexus | Analytics",
    'app.users_catalogue'       => "Nexus | Users",

    /**
     * app -> navbar (_nav)
     */
    'nav.title'                 => "Element",

    /**
     * app -> sidebar (_side)
     */
    'side.stat_online'          => "Online",
    'side.stat_away'            => "Offline",
    'side.search'               => "Søg...",
    'side.header'               => "Naviger",

    /**
     * app -> sidebar -> menu's (_menu)
     */
    /* menu : DASHBOARD */
    'menu.daily_view'           => "Dashboard",
    'menu.daily_1'              => "KPI's",
    'menu.daily_2'              => "Performance",
    /* menu : ORDER */
    'menu.order'                => "Ordrer",
    'menu.order_overview'       => "Overblik",
    /* menu : INVENTORY */
    'menu.inventory'            => "Lagerstyring",
    'menu.inventory_categories' => "Kategorier",
    'menu.inventory_groups'     => "Grupper",
    'menu.inventory_products'   => "Produkter",
    /* menu : BLOG */
    'menu.blog'                 => "Blogs",
    'menu.blog_overview'        => "Blogs",
    /* menu : USER */
    'menu.user'                 => "Brugere",
    'menu.user_profile'         => "Din profil",
    'menu.user_customer'        => "Kunder",
    'menu.user_worker'          => "Medarbejdere",
    /* menu : MONITOR */
    'menu.monitor'              => "Monitor",
    'menu.monitor_endpoints'    => "Endpoints",
    'menu.monitor_x'            => "??",
    /* menu : AUTH */
    'menu.lock_system'          => "Lås systemet",
    'menu.sign_out'             => "Log ud",
    'menu.sign_in'              => "Log ind",
    /* menu : SITE INFO */
    'menu.site_version'         => "Version",

    /**
     * app -> view -> DAILY:KPI
     */
    /* area : BREADCRUMB */
    'view.kpi.br_title'         => "Daily sales",
    'view.kpi.br_desc'          => "Optional description",
    'view.kpi.br_lvl_main'      => "home",
    'view.kpi.br_lvl_target'    => "KPI's",
    /**
     * app -> view -> MONITOR:ENDPOINTS
     */
    /* area : BREADCRUMB */
    'view.monitor.br_title'     => "Overvåg Endpoints",
    'view.monitor.br_desc'      => "Registrér URL endpoints til overvågning her!",
    'view.monitor.br_lvl_main'  => "home",
    'view.monitor.br_lvl_target'=> "monitor",

    /*--------------------------------------------------|
    |                                                   |
    |   AUTH -> USER -> ACTIVATE                        |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * app -> USER ACTIVATION (_app)
     */
    'app.user_acct_activate_headline'           => "Angiv koden som du har fået",
    'app.user_acct_activate_paragraf'           => "tilsendt i velkomstmailen",
    'app.user_acct_activate_token'              => "token",
    'app.user_acct_activate_resend'             => "Send igen",
    'app.user_acct_activate_customer'           => "kundeservice",

];
